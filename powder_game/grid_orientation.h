#pragma once

namespace powder
{

template<typename DimensionType = int>
class column_wise_grid
{
public:
	using dim_t = DimensionType;

	template<int Width, int Height>
	constexpr static auto at_position(const dim_t& x, const dim_t& y)
	{
		return (x * Height) + y;
	}

	template<int Width, int Height, typename Coordinate>
	constexpr static auto from_position(const dim_t xy)
	{
		return Coordinate{xy - (xy % Height), (xy % Height)};
	}
};

template<typename DimensionType = int>
class row_wise_grid
{
public:
	using dim_t = DimensionType;
	template<int Width, int Height>
	constexpr static auto at_position(const dim_t& x, const dim_t& y)
	{
		return (y * Width) + x;
	}

	template<int Width, int Height, typename Coordinate>
	constexpr static auto from_position(const dim_t xy)
	{
		return Coordinate{(xy % Width), xy - (xy % Width)};
	}
};
	
} // namespace powder
