#pragma once
#include "grid_orientation.h"
#include <stdexcept>
#include <map>
#include <iterator>

namespace powder
{

template<typename Particule, int Width, int Height, typename Orientation>
class map_grid_model
{
public:
	using dim_t = int;
	constexpr static dim_t width = Width;
	constexpr static dim_t height = Height;

	struct coordinate
	{
		int x = 0, 
		    y = 0;

		bool operator==(coordinate o) const noexcept 
		{
			return x == o.x && y == o.y;
		}
		bool operator!=(coordinate o) const noexcept 
		{
			return !operator==(o);
		}

		bool operator<(coordinate o) const noexcept 
		{
			return lesser_than(o, Orientation{});
		}
		bool lesser_than(coordinate o, column_wise_grid<>) const noexcept 
		{
			return y < o.y || (y == o.y && x < o.x);
		}
		bool lesser_than(coordinate o, row_wise_grid<>) const noexcept 
		{
			return x < o.x || (x == o.x && y < o.y);
		}
	};

	constexpr static bool in_boundary(const coordinate& pos)
	{
		return pos.x < Width && pos.x >= 0 && pos.y < Height && pos.y >= 0;
	}

	Particule& at(const coordinate& pos)
	{
		return map_.at(pos);
	}

	Particule const& at(const coordinate& pos) const
	{
		return map_.at(pos);
	}

	Particule get(const coordinate& pos) const
	{
		return map_.count(pos) > 0 ? at(pos) : Particule{};
	}

	void set(const coordinate& pos, Particule&& p)
	{
		map_.insert_or_assign(pos, p);
	}
	
	bool has(const coordinate& pos) const noexcept
	{
		return map_.count(pos) > 0;
	}

	void move(const coordinate from, const coordinate to)
	{
		if(in_boundary(to))
		{
			if(from != to) 
			{
				map_.insert_or_assign(to, map_.extract(from).mapped());
			}
		}
		else
		{
			at(from) = Particule{};
		}
	}

private:
	using grid_map = std::map<coordinate, Particule>;
	grid_map map_;

public:	

	class key_iterator {
		typename grid_map::iterator wrapped;
	public:
		using iterator_category	= std::forward_iterator_tag;
		using value_type = typename grid_map::mapped_type;
		using difference_type = typename grid_map::iterator::difference_type;
		using pointer = typename grid_map::mapped_type*;
		using reference = typename grid_map::mapped_type&;

		explicit key_iterator()
			: wrapped()
		{
		}
		explicit key_iterator(typename grid_map::iterator wrapped)
			: wrapped(wrapped)
		{
		}
        key_iterator& operator++() { ++wrapped; return *this;}
        key_iterator operator++(int) {key_iterator retval = *this; ++(*this); return retval;}
        bool operator==(key_iterator other) const {return wrapped == other.wrapped;}
        bool operator!=(key_iterator other) const {return wrapped != other.wrapped;}
        reference operator*() const { return (*wrapped).second; }
	};

	auto begin()
	{
		return key_iterator{std::begin(map_)};
	}

	auto end()
	{
		return key_iterator{std::end(map_)};
	}
};

}
