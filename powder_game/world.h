#pragma once

#include "Units.h"

#include "element.h"

namespace powder
{
template<int Width, int Height, typename Model, 
	typename Orientation, template<typename, int, int, typename> typename Grid>
class world
{
	static_assert(Width > 0, "The world Width must be over zero");
	static_assert(Height > 0, "The world Height must be over zero");
	
	using GridType = Grid<typename Model::particule, Width, Height, Orientation>;
	using particule = typename Model::particule;

	GridType world_grid_;

public:
	using dim_t = int;
	using coordinate = typename GridType::coordinate;

	constexpr static dim_t width = Width;
	constexpr static dim_t height = Height;

	explicit world() : world(element_type::air) {};
	explicit world(element_type t) : world_grid_{t} {};

	element_type get(const int x, const int y) const noexcept
	{
		return world_grid_.get({x,y}).element();
	}
	
	void set(const coordinate pos, const element_type elem)
	{
		if(world_grid_.in_boundary(pos)) 
		{
			world_grid_.set(pos, particule{pos.x, pos.y, elem});
		}
	}

	void update(const Second time_step)
	{
		Model::update<Width, Height, true>(time_step, world_grid_);
	}

};

} // namespace powder

