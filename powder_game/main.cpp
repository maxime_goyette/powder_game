#include <iostream>
#include <random>
#include <algorithm>

#include <allegro5/allegro.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>

#include "allegro/common.h"
#include "allegro/bitmap.h"
#include "world.h"

#include "array_grid_model.h"
#include "grid_orientation.h"
#include "physics_world_model.h"
#include "world_rendering.h"

constexpr static int screen_width = 450;
constexpr static int screen_height = 600;
constexpr static int bitmap_width = screen_width * 1;
constexpr static int bitmap_height = screen_height * 1;

using powder_world = powder::world<bitmap_width, bitmap_height, 
	powder::realistic_model, 
	powder::column_wise_grid<>, powder::array_grid_model>;

powder::element_type elem_current;
const char* elem_name[5] = { "air", "wall", "water", "steam", "ice"};

int main()
{
	elem_current = powder::element_type::air;
	double min_fps = 200;

	using namespace allegro;
	int mouseX = 0, mouseY = 0;

	powder_world the_world;
	const auto map_coordinate = powder::render::map_coordinate<powder_world::coordinate>;

	if (!al_init() || !al_install_mouse() || !al_install_keyboard() || !al_init_font_addon() || !al_init_ttf_addon()) {
		std::cerr << "failed to initialize allegro!\n";
		return EXIT_FAILURE;
	}

	display_ptr display{ al_create_display(screen_width, screen_height) };
	if (!display) {
		std::cerr << "failed to create display!\n";
		return EXIT_FAILURE;
	}

	event_queue_ptr event_queue{ al_create_event_queue() };
	if (!event_queue) {
		std::cerr << "failed to create event_queue!\n";
		return EXIT_FAILURE;
	}
	al_register_event_source(event_queue.get(), al_get_display_event_source(display.get()));
	al_register_event_source(event_queue.get(), al_get_mouse_event_source());
	al_register_event_source(event_queue.get(), al_get_keyboard_event_source());

	path_ptr font{ al_get_standard_path(ALLEGRO_RESOURCES_PATH) };
	al_set_path_filename(font.get(), "FiraCode-Medium.ttf");

	font_ptr ttf_font{ al_load_ttf_font(al_path_cstr(font.get(), ALLEGRO_NATIVE_PATH_SEP), 16, 0) };
	if (!ttf_font) {
		std::cerr << "failed to load verdana font!\n";
		return EXIT_FAILURE;
	}

	powder::render::element_colour colour;

	bitmap bitmap(bitmap_width, bitmap_height);
	if (!bitmap)
	{
		std::cerr << "failed to create bitmap!\n";
		return EXIT_FAILURE;
	}

	al_clear_to_color(colour[powder::element_type::air]);
	al_flip_display();

	bool close_window = false;
	bool mouse_down = false;

	static auto old_time = al_get_time();
	static auto new_time = al_get_time();
	while (!close_window)
	{
		static Second delta{};

		while (!al_event_queue_is_empty(event_queue.get()))
		{
			ALLEGRO_EVENT ev;
			const bool get_event = al_get_next_event(event_queue.get(), &ev);

			if (get_event) {

				switch (ev.type)
				{
				case ALLEGRO_EVENT_DISPLAY_CLOSE:
					close_window = true;
					break;
				case ALLEGRO_EVENT_MOUSE_AXES:
					if(mouse_down){
						the_world.set(map_coordinate(ev.mouse.x, ev.mouse.y, bitmap_width, bitmap_height), elem_current);
						mouseX = ev.mouse.x; mouseY = ev.mouse.y;
					}
					break;
				case ALLEGRO_EVENT_KEY_DOWN:
					switch (ev.keyboard.keycode) {
					case ALLEGRO_KEY_W:
						elem_current = powder::element_type::water;
						break;

					case ALLEGRO_KEY_F:
						elem_current = powder::element_type::wall;
						break;

					case ALLEGRO_KEY_S:
						elem_current = powder::element_type::steam;
						break;

					case ALLEGRO_KEY_I:
						elem_current = powder::element_type::ice;
						break;

					default: // nada
						break;
					}
					break;

				case ALLEGRO_EVENT_MOUSE_BUTTON_DOWN:
					mouse_down = true;
					std::cout<< ev.mouse.x << " " << ev.mouse.y << "\n";
					the_world.set(map_coordinate(ev.mouse.x, ev.mouse.y, bitmap_width, bitmap_height), elem_current); 
					break;
				case ALLEGRO_EVENT_MOUSE_BUTTON_UP:
					mouse_down = false;
					break;
				default:
					break;
				}
			}
		}
		if(mouse_down) 
		{
			the_world.set(map_coordinate(mouseX, mouseY, bitmap_width, bitmap_height), elem_current);
		}
		
		{
			scoped_bitmap_swap scope(bitmap, al_get_backbuffer(display.get()));
			scoped_bitmap_lock lock(bitmap);
	
			al_clear_to_color(al_map_rgb(255, 250, 250));
			for (int x = 0; x < powder_world::width; ++x) {
				for (int y = 0; y < powder_world::height; ++y) {
					const auto element = the_world.get(x,y);
					al_put_pixel(bitmap.width()-x-1, bitmap.height()-y-1, colour[element]);
				}
			}
		}

		const auto fps = 1.0 / (new_time - old_time);
		min_fps = std::min(fps, min_fps);
		//old_time = new_time;

		al_clear_to_color(al_map_rgb(255, 255, 255));
		al_draw_scaled_bitmap(bitmap, 0, 0, bitmap_width, bitmap_height, 0, 0, screen_width, screen_height, 0);
		al_draw_textf(ttf_font.get(), al_map_rgb(255, 255, 255), 0.0, 0.0, 0, "FPS : %f", fps);
		al_draw_textf(ttf_font.get(), al_map_rgb(255, 255, 255), 0.0, 15.0, 0, "FPS min : %f", min_fps);
		al_draw_textf(ttf_font.get(), colour[elem_current], 0.0, 30.0, 0, "%s", elem_name[index_of(elem_current)]);
		al_flip_display();
		
		old_time = new_time;
		new_time = al_get_time();
		delta = Second{new_time - old_time};

		the_world.update(Second{delta});
	}
}
