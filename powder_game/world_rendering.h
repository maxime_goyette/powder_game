#pragma once

#include "allegro/bitmap.h"
#include "element.h"

namespace powder::render
{

struct element_colour
{
	ALLEGRO_COLOR properties[index_of(element_type::boundary_flag)];

	element_colour()
	{
		properties[0] = al_map_rgb(0,0,0);
		properties[1] = al_map_rgb(100,100,100);
		properties[2] = al_map_rgb(0,0,255);
		properties[3] = al_map_rgb(100,100,255);
		properties[4] = al_map_rgb(200,200,255);
	}

	ALLEGRO_COLOR const& operator[](const element_type type) const noexcept
	{
		return properties[index_of(type)];
	}
};

template<typename Coordinate>
Coordinate map_coordinate(const int x, const int y, const int width, const int height)
{
	return {width-x-1, height-y-1};
}

template<typename World>
void render(World& w, bitmap& bitmap, allegro::display_ptr& display)
{
	static element_colour colour;
	scoped_bitmap_swap scope(bitmap, al_get_backbuffer(display.get()));
	scoped_bitmap_lock lock(bitmap);
	
	al_clear_to_color(colour[element_type::air]);
	for (int x = 0; x < World::width; ++x) {
		for (int y = 0; y < World::height; ++y) {
			const auto element = w.get(x,y);
			if(element != element_type::air) 
			{
				al_put_pixel(bitmap.width()-x-1, bitmap.height()-y-1, colour[element]);
			}
		}
	}
}
	
}
