#pragma once

#include "common.h"

class bitmap {
	using struct_type = ALLEGRO_BITMAP;
	using pointer_type = struct_type*;
	using owned_pointer_type = allegro::unique_ptr<struct_type, al_destroy_bitmap>;

	using dim_t = int;

	owned_pointer_type m_ptr;
	dim_t m_width, m_height;

public:
	bitmap(const int width, const int height) noexcept :
		m_ptr{ al_create_bitmap(width, height) },
		m_width{ width },
		m_height{ height }
	{}

	bitmap(const pointer_type stolen, const int width, const int height) noexcept :
		m_ptr{ stolen },
		m_width{ width },
		m_height{ height }
	{}

	bitmap(bitmap&& other) noexcept
    : m_ptr(std::move(other.m_ptr)),
	  m_width(other.m_width),
 	  m_height(other.m_height)
	{
	}

	bitmap& operator=(bitmap&& other) noexcept
	{
		if (this == &other)
			return *this;
		m_ptr = std::move(other.m_ptr);
		m_width = other.m_width;
		m_height = other.m_height;
		return *this;
	}

	/*implicit*/ operator pointer_type () const noexcept {
		return m_ptr.get();
	}

	bitmap(const bitmap& other) = delete;

	bitmap& operator=(const bitmap& other) = delete;

	virtual ~bitmap() = default;

	dim_t width() const noexcept { return m_width; }
	dim_t height() const noexcept { return m_height; }
};

class scoped_bitmap_lock 
{
	using pointer_type = ALLEGRO_BITMAP*;
	pointer_type resource_;
public:
	explicit scoped_bitmap_lock(const pointer_type res, const int flag = ALLEGRO_LOCK_WRITEONLY) noexcept
	: resource_(res)
	{
		al_lock_bitmap(resource_, al_get_bitmap_format(resource_), flag);
	}

	scoped_bitmap_lock(const scoped_bitmap_lock& other) = delete;

	scoped_bitmap_lock(scoped_bitmap_lock&& other) noexcept = delete;

	scoped_bitmap_lock& operator=(const scoped_bitmap_lock& other) = delete;

	scoped_bitmap_lock& operator=(scoped_bitmap_lock&& other) noexcept = delete;

	virtual ~scoped_bitmap_lock() noexcept
	{
		al_unlock_bitmap(resource_);
	}
};

class scoped_bitmap_swap 
{
	using pointer_type = ALLEGRO_BITMAP*;
	pointer_type resource_;
public:
	explicit scoped_bitmap_swap(const pointer_type next, const pointer_type restore) noexcept 
	: resource_(restore)
	{
		al_set_target_bitmap(next);
	}
	scoped_bitmap_swap(const scoped_bitmap_swap& other) = delete;

	scoped_bitmap_swap(scoped_bitmap_swap&& other) noexcept = delete;

	scoped_bitmap_swap& operator=(const scoped_bitmap_swap& other) = delete;

	scoped_bitmap_swap& operator=(scoped_bitmap_swap&& other) noexcept = delete;

	virtual ~scoped_bitmap_swap() noexcept
	{
		al_set_target_bitmap(resource_);
	}
};
