#ifndef POWDER_PHYSICS_SIMULATION_HPP
#define POWDER_PHYSICS_SIMULATION_HPP

#include "Units.h"

#include "element.h"
#include "physics_traits.h"

#include <algorithm>
#include <optional>

#if __has_include ( <execution> )
#define POWDER_HAS_PARALLEL_EXECUTION
#include <execution>

using default_execution_policy = std::execution::parallel_unsequenced_policy;
#else
struct default_execution_policy{};
#endif


namespace powder::constants
{
	using namespace unit_literal;

	constexpr static divide_t<Newton, Kilogram> gravity{9.8};
	constexpr static long double pi{3.14159265358979323846264338327950288419716939937510582097494459230781640628620899862803482534211706798214808651e+00};
}

namespace powder::physics
{
	using namespace unit_literal;

    /**
	 * \param element le type de l'�l�ment
	 * \param volume  le volume de la particule
	 * \return la masse de la particule � partir de sa masse volumique
	 */
	constexpr Kilogram mass(const element_type element, const Metre3& volume = 1.0_m3)
	{
		const Density rho = elements[element].density;
		return {rho * volume};
	}
	
    /**
	 * \param element       le type d'�l�ment de la particule
	 * \param outer_element le type de l'�l�ment entourant la particule
	 * \return la pouss�e verticale de la particule � l'int�rieur de outer_element
	 */
	constexpr Vector<Newton> buoyancy(const element_type element, const element_type outer_element = element_type::air)
	{
		if(element == outer_element)
		{
			return {};
		}
		// Buoyancy = force on displaced outer element
		return {0.0_N, (mass(outer_element) * constants::gravity), 0.0_N};
	}

	/**
	 * \param element le type de l'�l�ment
	 * \return la force gravitationnelle sur 1m� de l'�l�ment
	 */
	constexpr Vector<Newton> gravity(const element_type element)
	{
		return {0.0_N, -(mass(element) * constants::gravity), 0.0_N};
	}

	/**
	 * \return l'air de la coupe transversale d'un cercle
	 */
	constexpr Metre2 cross_sectional_area()
	{
		//(1/4)*pi*D^2
		return .25 * constants::pi * 1.0_m * 1.0_m;
	}

	/**
	 * \param velocity le vecteur de vitesse actuel de la particule
	 * \return la force de la tra�n�e de la particule
	 */
	inline Vector<Newton> quadratic_air_drag(const Vector<Speed>& velocity)
	{
		// http://dynref.engr.illinois.edu/afp.html
		// Fd = 1/2*Cd*p*A*v^2*v_norm
		constexpr double c_d = 0.47; // https://en.wikipedia.org/wiki/Drag_coefficient
		constexpr Density r = elements[element_type::air].density;
		constexpr Metre2 a = cross_sectional_area();

		const Speed x {velocity.x()};
		const Speed y {velocity.y()};
		const Vector<multiply_t<Speed, Speed>> v2(x * x, y * y, multiply_t<Speed, Speed>{0.0});
		const Vector<Scalar> normalized = velocity.normalized();
		const Vector<Newton> fd = (0.5 * c_d) * r * a * v2 * normalized;

		return fd;
	}

	template<typename Particule>
	void euler_method(Particule& p, const Second time_step) noexcept
	{
		p.acceleration = p.force / physics::mass(p.element);
		p.velocity += p.acceleration * time_step;
		p.position += p.velocity * time_step;
	}
	
	template<typename Particule>
	void velocity_verlet_integration(Particule& p, const Second time_step) noexcept
	{
		const Vector<Metre> delta_position{p.velocity() * time_step + ( 0.5 * time_step * time_step * p.acceleration())};
		const Vector<Acceleration> new_acceleration{p.force() / physics::mass(p.element())};
		const Vector<Acceleration> avg_acceleration{( p.acceleration() + new_acceleration ) / 2};
		const Vector<Speed> delta_velocity{avg_acceleration * time_step};
		
		p.set_acceleration(new_acceleration);
		p.set_velocity(delta_velocity);
		p.set_new_position(p.new_position() + delta_position);
	}

	class particule
	{
		element_type element_;
		Vector<Metre> position_;
		Vector<Metre> new_position_;
		Vector<Newton> force_;
		Vector<Acceleration> acceleration_;
		Vector<Speed> velocity_;
		
	public:
		explicit particule(const particule& other) = default;

		particule(particule&& other) noexcept = delete;
		particule& operator=(particule&& other) noexcept = delete;

		explicit particule(int x, int y, const element_type e = element_type::air) noexcept 
		: element_{e}, 
		  position_(Metre{static_cast<double>(x)}, Metre{static_cast<double>(y)}, Metre{}),
		  new_position_(position_)
		{}

		explicit particule(double x, double y, const element_type e = element_type::air) noexcept 
		: element_{e},
		  position_(Metre{x}, Metre{y}, Metre{}),
		  new_position_(position_)
		{}

		particule& operator=(const particule& other) = delete;

		static void swap(particule& p1, particule& p2)
		{
			std::swap(p1.element_, p2.element_);
			std::swap(p1.position_, p2.position_);
			std::swap(p1.acceleration_, p2.acceleration_);
			std::swap(p1.velocity_, p2.velocity_);
			std::swap(p1.position_, p2.position_);
			p1.set_new_position(p1.position());
			p2.set_new_position(p2.position());
		}

		constexpr element_type element() const noexcept
		{
			return element_;
		}

		constexpr void set_element(const element_type n_type) noexcept
		{
			element_ = n_type;
		}

		constexpr Metre x() const noexcept
		{
			return position_.x();
		}
		
		constexpr Metre y() const noexcept
		{
			return position_.y();
		}

		constexpr Metre new_x() const noexcept
		{
			return new_position_.x();
		}
		
		constexpr Metre new_y() const noexcept
		{
			return new_position_.y();
		}

		constexpr const Vector<Metre>& position() const noexcept
		{
			return position_;
		}

		constexpr void set_position(const Vector<Metre>& n_position) noexcept
		{
			position_ = n_position;
			new_position() = position_;
		}

		constexpr const Vector<Metre>& new_position() const noexcept
		{
			return new_position_;
		}

		constexpr Vector<Metre>& new_position() noexcept
		{
			return new_position_;
		}

		constexpr void set_new_position(const Vector<Metre>& n_position) noexcept
		{
			new_position_ = n_position;
		}

		constexpr const Vector<Newton>& force() const noexcept
		{
			return force_;
		}

		constexpr Vector<Newton>& force() noexcept
		{
			return force_;
		}

		constexpr void set_force(const Vector<Newton>& n_force) noexcept
		{
			force_ = n_force;
		}

		constexpr const Vector<Acceleration>& acceleration() const noexcept
		{
			return acceleration_;
		}

		constexpr Vector<Acceleration>& acceleration() noexcept
		{
			return acceleration_;
		}

		constexpr void set_acceleration(const Vector<Acceleration>& n_acceleration) noexcept
		{
			acceleration_ = n_acceleration;
		}

		constexpr const Vector<Speed>& velocity() const noexcept
		{
			return velocity_;
		}

		constexpr Vector<Speed>& velocity() noexcept
		{
			return velocity_;
		}

		constexpr void set_velocity(const Vector<Speed>& n_velocity) noexcept
		{
			velocity_ = n_velocity;
		}
	};
}

namespace powder
{


class realistic_model
{
public:
	enum class collision
	{
		can_move,
		cannot_move
	};

	using particule = physics::particule;
	
private:
	template<typename WorldSpace>
	static void update_force(particule& p) noexcept
	{
		p.set_force(physics::gravity(p.element()) + 
			        physics::buoyancy(p.element()) + 
				    physics::quadratic_air_drag(p.velocity()));
	}
	
	template<typename WorldSpace>
	static void update_kinetic(const Second time_step, particule& p) noexcept
	{
		physics::velocity_verlet_integration(p, time_step);
	}

	static collision handle_collision(const particule& from, const particule& to)
	{
		if (to.element() == element_type::air)
		{
			return collision::can_move;
		}
		if(from.element() == to.element())
		{
			return collision::cannot_move;
		}
		
		const Density dFrom = physics::elements[from.element()].density;
		const Density dTo   = physics::elements[to.element()].density;
		if (dTo < dFrom)
		{
			return collision::can_move;
		}

		return collision::cannot_move;
	}

	template<typename WorldSpace, bool DoCollision>
	static void update_position(WorldSpace& world)
	{
		using coordinate = typename WorldSpace::coordinate;
		for (auto x = 0; x < WorldSpace::width; ++x)
		{
			for (auto y = 0; y < WorldSpace::height; ++y)
			{
				coordinate coor{ x,y };
				particule& p = world.at(coor);
				if(p.element() == element_type::air)
				{
					continue;
				}

				auto new_coordinate = coordinate{ static_cast<int>(p.new_x().Value()),
												  static_cast<int>(p.new_y().Value()) };
				if(new_coordinate == coor)
				{
					continue;
				}
				//Collisions => Reduction du mouvement
				if (world.in_boundary(new_coordinate) 
					&& collision::can_move == handle_collision(p, world.at(new_coordinate))) 
				{
					world.move(coor, new_coordinate);
				}
				else 
				{
					if constexpr(DoCollision) 
					{
						if(const auto available = try_falling(world, p, coor, new_coordinate); available)
						{
							world.move(coor, *available);
						}
						else
						{
							p.set_velocity(Vector<Speed>{});
							p.set_new_position(p.position());
						}
					}
					else
					{
						p.set_velocity(Vector<Speed>{});
						p.set_new_position(p.position());
					}
				}
			}
		}
	}

	template<typename WorldSpace, typename Coordinate>
	static std::optional<Coordinate> try_falling(const WorldSpace& world, const particule& p, const Coordinate& current, const Coordinate& next) 
	{
		switch (physics::elements[p.element()].state) 
		{
		case element_state::liquid:
			return try_falling_liquid(world, p, current, next);
		case element_state::solid:
			return try_falling_solid(world, p, current, next);
		case element_state::gaz:
		default:
			return {};
		}
	}

	template<typename WorldSpace, typename Coordinate>
	static std::optional<Coordinate> try_falling_solid(const WorldSpace& world, const particule& p, const Coordinate& current, const Coordinate& next) 
	{
		// remove left/right bias
		static auto next_side = 1;
		const auto side = next_side == 1 ? -1 : 1;
		next_side = side;

		// the particules should stabilize as pyramids
		auto[new_x, new_y] = next;
		auto[x, y] = current;
		if (const auto left = Coordinate{ new_x + side, new_y - 1 };
			world.in_boundary(left) && collision::can_move == handle_collision(p, world.at(left)))
		{
			return left;
		}
		if (const auto right = Coordinate{ new_x - side, new_y - 1 };
		    world.in_boundary(right) && collision::can_move == handle_collision(p, world.at(right)))
		{
			return right;
		}
		return {};
	}

	template<typename WorldSpace, typename Coordinate >
	static std::optional<Coordinate> try_falling_liquid(const WorldSpace& world, const particule& p, const Coordinate& current, const Coordinate& next) 
	{
		// remove left/right bias
		static auto next_side = 1;
		const auto side = next_side == 1 ? -1 : 1;
		next_side = side;

		// the particules should stabilize as flatly as possible
		auto[new_x, new_y] = next;
		auto[x, y] = current;
		
		if (const auto n_b_left = Coordinate{ new_x - side, new_y - 1 };  
		    world.in_boundary(n_b_left) && collision::can_move == handle_collision(p, world.at(n_b_left)))
		{
			return n_b_left;
		}
		if (const auto n_b_right = Coordinate{ new_x + side, new_y - 1 };  
		    world.in_boundary(n_b_right) && collision::can_move == handle_collision(p, world.at(n_b_right)))
		{
			return n_b_right;
		}
		if (const auto n_left = Coordinate{ new_x - side, new_y };  
		    world.in_boundary(n_left) && collision::can_move == handle_collision(p, world.at(n_left)))
		{
			return n_left;
		}
		if (const auto n_right = Coordinate{ new_x + side, new_y };  
		    world.in_boundary(n_right) && collision::can_move == handle_collision(p, world.at(n_right)))
		{
			return n_right;
		}
		if (const auto b_left = Coordinate{ x + side, y - 1 }; 
			world.in_boundary(b_left) && collision::can_move == handle_collision(p, world.at(b_left)))
		{
			return b_left;
		}
		if (const auto b_right = Coordinate{ x - side, y - 1 };  
		    world.in_boundary(b_right) && collision::can_move == handle_collision(p, world.at(b_right)))
		{
			return b_right;
		}
		if (const auto left = Coordinate{ x + side, y }; 
			world.in_boundary(left) && collision::can_move == handle_collision(p, world.at(left)))
		{
			return left;
		}
		if (const auto right = Coordinate{ x - side, y };  
		    world.in_boundary(right) && collision::can_move == handle_collision(p, world.at(right)))
		{
			return right;
		}
		return {};
	}

public:
	template<int Width, int Height, bool DoCollision = false, typename ExecutionPolicy = default_execution_policy, typename WorldSpace>
	static void update(const Second time_step, WorldSpace& world)
	{		
		auto update_physics = [time_step](particule& p)
		{
			if(p.element() != element_type::air)
			{
				update_force<WorldSpace>(p);
				update_kinetic<WorldSpace>(time_step, p);
			}
		};

#ifdef POWDER_HAS_PARALLEL_EXECUTION
		std::for_each(ExecutionPolicy{}, world.begin(), world.end(), update_physics);
#else
		std::for_each(world.begin(), world.end(), update_physics);
#endif

		update_position<WorldSpace, DoCollision>(world);
	}
};
}

#endif //POWDER_PHYSICS_SIMULATION_HPP
