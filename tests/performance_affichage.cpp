#include "test_common.h"

TEST_CASE("Performance - Affichage - World Full of air", "[world]")
{
	using world_type = world<screen_width, screen_height, realistic_model, column_wise_grid<>, array_grid_model>;

	world_type w{element_type::air};

	if (!al_init() || !al_install_mouse() || !al_install_keyboard() || !al_init_font_addon() || !al_init_ttf_addon())
	{
		FAIL("failed to initialize allegro!");
	}

	allegro::display_ptr display{al_create_display(screen_width, screen_height)};
	if (!display)
	{
		FAIL("failed to create display!");
	}

	bitmap bitmap(screen_width, screen_height);
	if (!bitmap)
	{
		FAIL("failed to create bitmap!");
	}

	do_benchmark(nbr_iteration_world, [&]()
	{
		powder::render::render(w, bitmap, display);
		al_draw_scaled_bitmap(bitmap, 0, 0, screen_width, screen_height, 0, 0, screen_width, screen_height, 0);
		al_flip_display();
	});
}

TEST_CASE("Performance - Affichage - World Full of water", "[world]")
{
	using world_type = powder::world<screen_width, screen_height, realistic_model, column_wise_grid<>, array_grid_model>;

	world_type w{element_type::water};

	if (!al_init() || !al_install_mouse() || !al_install_keyboard() || !al_init_font_addon() || !al_init_ttf_addon())
	{
		FAIL("failed to initialize allegro!");
	}

	allegro::display_ptr display{al_create_display(screen_width, screen_height)};
	if (!display)
	{
		FAIL("failed to create display!");
	}

	bitmap bitmap(screen_width, screen_height);
	if (!bitmap)
	{
		FAIL("failed to create bitmap!");
	}

	do_benchmark(nbr_iteration_world, [&]()
	{
		powder::render::render(w, bitmap, display);
		al_draw_scaled_bitmap(bitmap, 0, 0, screen_width, screen_height, 0, 0, screen_width, screen_height, 0);
		al_flip_display();
	});
}