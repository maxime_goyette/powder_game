#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include <catch.hpp>

#include "../powder_game/physics_world_model.h"
using namespace unit_literal;
using namespace powder;

TEST_CASE("mass is correctly calculated for 1m3", "[physics]") {
	REQUIRE( physics::mass(element_type::ice) == 916.7_kg);
}

TEST_CASE("mass is correctly calculated for x volume", "[physics]") {
	REQUIRE( physics::mass(element_type::ice, 2.0_m3) == 2*916.7_kg);
}

TEST_CASE("gravity is correctly calculated", "[physics]") {
	REQUIRE( physics::gravity(element_type::ice) == Vector<Newton>{0.0_N, -8983.66_N, 0.0_N});
}

/*
TEST_CASE("buoyancy is correctly calculated", "[physics]") {
	REQUIRE( physics::buoyancy(element_type::ice) == Vector<Newton>{0.0_N, -8983.66_N, 0.0_N});
}

TEST_CASE("buoyancy is correctly calculated for other type", "[physics]") {
	REQUIRE( physics::buoyancy(element_type::ice, element_type::water) == Vector<Newton>{0.0_N, -8983.66_N, 0.0_N});
}

TEST_CASE("cross_section is correctly calculated", "[physics]") {
	REQU
IRE( physics::cross_sectional_area() == 0.0_m2);
}
*/
//TEST_CASE("quadratic_air_drag is correctly calculated") {
//	using namespace unit_literal;
//	CHECK( physics::quadratic_air_drag(ice{}) == Vector<Newton>{0.0_N, -8983.66_N, 0.0_N});
//}
//
//TEST_CASE("quadratic_air_drag is correctly calculated") {
//	using namespace unit_literal;
//	CHECK( physics::quadratic_air_drag(ice{}) == Vector<Newton>{0.0_N, -8983.66_N, 0.0_N});
//}