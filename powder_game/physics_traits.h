#ifndef POWDER_PHYSICS_TRAITS_HPP
#define POWDER_PHYSICS_TRAITS_HPP

#include "Units.h"
#include "element.h"

namespace powder::physics
{

struct element_properties
{
	element_type type;
	element_state state;
	Density density;
};

struct element_info
{
	constexpr static element_properties properties[] {
		{ element_type::air,   element_state::gaz,    Density{1.20} },    
		{ element_type::wall,  element_state::solid,  Density{2'400.0} }, 
		{ element_type::water, element_state::liquid, Density{1'000.0} }, 
		{ element_type::steam, element_state::gaz,    Density{0.1} },     
		{ element_type::ice,   element_state::solid,  Density{916.7} },   
	};
	static_assert(sizeof(properties)/sizeof(element_properties) == index_of(element_type::boundary_flag), 
		"Missing element_info in powder::physics::elements");

	constexpr element_properties const& operator[](const element_type type) const noexcept
	{
		return properties[index_of(type)];
	}
};

constexpr static element_info elements{};

} // namespace powder

#endif // POWDER_PHYSICS_TRAITS_HPP
