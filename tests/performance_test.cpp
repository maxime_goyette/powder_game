#include "test_common.h"

using namespace powder;

TEST_CASE("Performance - Seq - World Full of water", "[world]")
{
	using model = realistic_model;
	using grid = array_grid_model<model::particule, screen_width, screen_height, column_wise_grid<>>;

	grid g{element_type::water};

	do_benchmark(nbr_iteration_world, [&g]()
	{
#ifdef POWDER_HAS_PARALLEL_EXECUTION
		model::update<screen_width, screen_height, false, std::execution::sequenced_policy>(Second{0.1}, g);
#else
		model::update<screen_width, screen_height, false>(Second{0.1}, g);
#endif
	});
}

TEST_CASE("Performance - Seq - World Full of air", "[world]")
{
	using model = realistic_model;
	using grid = array_grid_model<model::particule, screen_width, screen_height, column_wise_grid<>>;

	grid g{element_type::air};

	do_benchmark(nbr_iteration_world, [&g]()
	{
#ifdef POWDER_HAS_PARALLEL_EXECUTION
		model::update<screen_width, screen_height, false, std::execution::sequenced_policy>(Second{0.1}, g);
#else
		model::update<screen_width, screen_height, false>(Second{0.1}, g);
#endif
	});
}

TEST_CASE("Performance - Seq - World Full of water - Collision", "[world]")
{
	using model = realistic_model;
	using grid = array_grid_model<model::particule, screen_width, screen_height, column_wise_grid<>>;

	grid g{element_type::water};

	do_benchmark(nbr_iteration_world, [&g]()
	{
#ifdef POWDER_HAS_PARALLEL_EXECUTION
		model::update<screen_width, screen_height, true, std::execution::sequenced_policy>(Second{0.1}, g);
#else
		model::update<screen_width, screen_height, true, default_execution_policy>(Second{0.1}, g);
#endif
	});
}

TEST_CASE("Performance - Seq - World Full of air - Collision", "[world]")
{
	using model = realistic_model;
	using grid = array_grid_model<model::particule, screen_width, screen_height, column_wise_grid<>>;

	grid g{element_type::air};

	do_benchmark(nbr_iteration_world, [&g]()
	{
#ifdef POWDER_HAS_PARALLEL_EXECUTION
		model::update<screen_width, screen_height, true, std::execution::sequenced_policy>(Second{0.1}, g);
#else
		model::update<screen_width, screen_height, true, default_execution_policy>(Second{0.1}, g);
#endif
	});
}
