#pragma once

#include <memory>

#include "common.h"

#include <allegro5/allegro.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>

namespace allegro {



	

	class scoped_bitmap_lock {
		using pointer_type = ALLEGRO_BITMAP * ;
		pointer_type resource;
	public:
		scoped_bitmap_lock(pointer_type res, int flag = ALLEGRO_LOCK_WRITEONLY) noexcept : resource(res)
		{
			al_lock_bitmap(resource, al_get_bitmap_format(resource), flag);
		}
		virtual ~scoped_bitmap_lock() noexcept
		{
			al_unlock_bitmap(resource);
		}
	};

	template<typename T, typename Fct>
	class scoped_ressource_swap {
		T resource;
		Fct swapper;
	public:
		scoped_ressource_swap(T next, T restore) : resource(restore)
		{
			swapper(next);
		}
		virtual ~scoped_ressource_swap() noexcept
		{
			swapper(resource);
		}
	};
	using scoped_bitmap_swap = scoped_ressource_swap<ALLEGRO_BITMAP*, free_function_functor<ALLEGRO_BITMAP, al_set_target_bitmap>>;


}