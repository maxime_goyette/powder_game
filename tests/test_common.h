#pragma once

#include <iostream>
#include <chrono>
#include <allegro5/allegro5.h>

#include "../powder_game/physics_traits.h"
#include "../powder_game/physics_world_model.h"
#include "../powder_game/array_grid_model.h"
#include "../powder_game/grid_orientation.h"

#include "../powder_game/allegro/common.h"
#include "../powder_game/allegro/bitmap.h"
#include "../powder_game/world_rendering.h"
#include "../powder_game/world.h"

#define CATCH_CONFIG_ENABLE_CHRONO_STRINGMAKER  
#include <catch.hpp>

using namespace powder;
constexpr static int nbr_iteration_world = 10'000;
constexpr static int screen_width = 640;
constexpr static int screen_height = 480;

/*
 * Tir� de � Mesurer le passage du temps �, Patrice Roy
 * http://h-deb.clg.qc.ca/Sujets/AuSecours/Mesurer-le-temps.html
 */
template <typename Lambda>
void do_benchmark(int NTESTS, Lambda fct)
{
	using namespace std::chrono;

	system_clock::duration temps_total = {},
	                       temps_min = {},
	                       temps_max = {};
	for (int cpt = 0; cpt < NTESTS;)
	{
		const auto avant = system_clock::now();

		fct();

		const auto apres = system_clock::now();
		const auto ecoule = duration_cast<milliseconds>(apres - avant);

		if (ecoule.count() >= 0)
		{
			temps_total += ecoule;
			if (!cpt)
				temps_min = temps_max = ecoule;
			else
			{
				if (ecoule < temps_min)
					temps_min = ecoule;
				if (ecoule > temps_max)
					temps_max = ecoule;
			}
			++cpt;
		}
	}

	const auto moyenne = duration_cast<milliseconds>(temps_total).count() / static_cast<double>(NTESTS);
	std::cout << "[ Statistiques ]" << std::endl
		<< "Temps minimal: " << duration_cast<milliseconds>(temps_min).count() << " ms." << std::endl
		<< "Temps maximal: " << duration_cast<milliseconds>(temps_max).count() << " ms." << std::endl
		<< "Temps moyen: " << moyenne << " ms." << std::endl;
}
