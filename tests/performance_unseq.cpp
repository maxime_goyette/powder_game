#include "test_common.h"

TEST_CASE("Performance - ParUnseq - World Full of water", "[world]")
{
#ifdef POWDER_HAS_PARALLEL_EXECUTION
	using model = realistic_model;
	using grid = array_grid_model<model::particule, screen_width, screen_height, column_wise_grid<>>;

	grid g{element_type::water};

	do_benchmark(nbr_iteration_world, [&g]()
	{
		model::update<screen_width, screen_height, false, std::execution::parallel_unsequenced_policy>(Second{0.1}, g);
	});
#endif
}

TEST_CASE("Performance - ParUnseq - World Full of air", "[world]")
{
#ifdef POWDER_HAS_PARALLEL_EXECUTION
	using model = realistic_model;
	using grid = array_grid_model<model::particule, screen_width, screen_height, column_wise_grid<>>;

	grid g{element_type::air};

	do_benchmark(nbr_iteration_world, [&g]()
	{
		model::update<screen_width, screen_height, false, std::execution::parallel_unsequenced_policy>(Second{0.1}, g);
	});
#endif
}

TEST_CASE("Performance - ParUnseq - World Full of water - Collision", "[world]")
{
#ifdef POWDER_HAS_PARALLEL_EXECUTION
	using model = realistic_model;
	using grid = array_grid_model<model::particule, screen_width, screen_height, column_wise_grid<>>;

	grid g{element_type::water};

	do_benchmark(nbr_iteration_world, [&g]()
	{
		model::update<screen_width, screen_height, true, std::execution::parallel_unsequenced_policy>(Second{0.1}, g);
	});
#endif
}

TEST_CASE("Performance - ParUnseq - World Full of air - Collision", "[world]")
{
#ifdef POWDER_HAS_PARALLEL_EXECUTION
	using model = realistic_model;
	using grid = array_grid_model<model::particule, screen_width, screen_height, column_wise_grid<>>;

	grid g{element_type::air};

	do_benchmark(nbr_iteration_world, [&g]()
	{
		model::update<screen_width, screen_height, true, std::execution::parallel_unsequenced_policy>(Second{0.1}, g);
	});
#endif
}
