# powder_game

# Setup

Le projet utilise [vcpkg](https://github.com/Microsoft/vcpkg) pour compiler et installer les dépendances graphiques. Les instructions pour l'installer sont décrit dans la page. 

Le projet dépend de:
 - allegro5 (graphique)
 - catch2 (pour les tests)

Les bibliothèques sont installés en x86 par défaut. Si vous préférez travailler en 64 bits, rajoutez
--triplet x64-windows dans la commande pour les installer.

ex:
  vcpkg install allegro5 catch2 --triplet x64-windows
ou:
  vcpkg install allegro5:x64-windows catch2:x64-windows
