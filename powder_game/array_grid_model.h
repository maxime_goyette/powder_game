#pragma once

#include <stdexcept>
#include <vector>


namespace powder
{

template<typename Particule, int Width, int Height, typename Orientation>
class array_grid_model
{
public:
	using dim_t = int;
	constexpr static dim_t width = Width;
	constexpr static dim_t height = Height;

	struct coordinate
	{
		int x = 0,
		    y = 0;

		bool operator==(const coordinate o) const noexcept 
		{
			return x == o.x && y == o.y;
		}
		bool operator!=(const coordinate o) const noexcept {
			return !operator==(o);
		}
	};

	constexpr dim_t at_position(const coordinate& pos) const noexcept
	{
		return Orientation::at_position<Width, Height>(pos.x, pos.y);
	}

	constexpr static bool in_boundary(const coordinate& pos) noexcept
	{
		return pos.x < Width && pos.x >= 0 && pos.y < Height && pos.y >= 0;
	}

	Particule& at(const coordinate& pos)
	{
		if(!in_boundary(pos)) 
		{
			throw std::out_of_range{ "Illegal access to particule" };
		}

		return map_[at_position(pos)];
	}

	Particule const& at(const coordinate& pos) const
	{
		if(!in_boundary(pos)) 
		{
			throw std::out_of_range{ "Illegal access to particule" };
		}

		return map_[at_position(pos)];
	}

	Particule get(const coordinate& pos) const
	{
		return Particule{at(pos)};
	}

	void set(const coordinate& pos, Particule&& p)
	{
		auto idx = at_position(pos);
		map_[idx].set_element(p.element());
		map_[idx].set_force(p.force());
		map_[idx].set_acceleration(p.acceleration());
		map_[idx].set_velocity(p.velocity());
	}

	bool has(const coordinate& pos) const noexcept
	{
		return in_boundary(pos);
	}

	void move(const coordinate from, const coordinate to)
	{
		if(in_boundary(to))
		{
			if(from != to) 
			{
				Particule::swap(at(to), at(from));
			}
		}
		else
		{
			at(from).set_element(element_type::air);
		}
	}

	auto begin() noexcept
	{
		return std::begin(map_);
	}

	auto end() noexcept
	{
		return std::end(map_);
	}

	explicit constexpr array_grid_model() 
		: array_grid_model(element_type::air)
	{};

	explicit constexpr array_grid_model(const element_type t) 
		: map_{ Width * Height, Particule{0.0, 0.0, t} }
	{
		for(auto y = 0; y < Height; ++y)
		{
			for(auto x = 0; x < Width; ++x)
			{
				map_[at_position({x,y})].set_position(Vector<Metre>(
					Metre{static_cast<double>(x)}, 
					Metre{static_cast<double>(y)}));
			}
		}
	}

private:
	std::vector<Particule> map_;
};

}
