﻿#pragma once

#include <memory>

#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>

namespace allegro {

template<typename T, void(*Fct)(T*)>
struct cleanup_function_functor {
	void operator()(T* ptr) const {
		Fct(ptr);
	}
};

template<typename T, void(*Fct)(T*)>
using unique_ptr = std::unique_ptr<T, cleanup_function_functor<T, Fct>>;

using display_ptr = unique_ptr<ALLEGRO_DISPLAY, al_destroy_display>;
using event_queue_ptr = unique_ptr<ALLEGRO_EVENT_QUEUE, al_destroy_event_queue>;
using path_ptr = unique_ptr<ALLEGRO_PATH, al_destroy_path>;
using font_ptr = unique_ptr<ALLEGRO_FONT, al_destroy_font>;

}