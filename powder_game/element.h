#ifndef POWDER_ELEMENT_HPP
#define POWDER_ELEMENT_HPP

#include <variant>

namespace powder 
{

enum class element_state
{
	solid, liquid, gaz
};

/**
 * Les types d'�l�ment disponible. 
 */
enum class element_type : unsigned char
{
	air = 0,
	wall,
	water,
	steam,
	ice,

	boundary_flag /* must be the last member */
};

constexpr static std::underlying_type<element_type>::type index_of(const element_type t)
{
	return static_cast<std::underlying_type<element_type>::type>(t);
}

} // namespace powder

#endif // POWDER_ELEMENT_HPP
